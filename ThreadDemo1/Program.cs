﻿using System;
using System.Threading;

namespace ThreadDemo1
{
    class Program
    {
        private const int NbrOfThreads = 20000;
        private const int SleepInMs = 20000;

        static void Main(string[] args)
        {
            Thread lastThread = null;

            Console.WriteLine("Creating {0} threads..", NbrOfThreads);
            for (var i = 0; i <= 20000; i++)
            {
                Thread thread = new Thread(Test) { IsBackground = true };

                thread.Start();
                lastThread = thread;
            }

            Console.WriteLine("Done. Please wait for at least {0}ms.", SleepInMs);
            if (lastThread != null)
            {
                lastThread.Join();
                Console.WriteLine("Last thread is now done. Hope the others are as well.");
            }
            
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }

        public static void Test()
        {
            Thread.Sleep(SleepInMs);
        }
    }
}
