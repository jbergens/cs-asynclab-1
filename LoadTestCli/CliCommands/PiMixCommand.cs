﻿using System;
using System.Collections.Generic;
using System.Text;
using LoadTestCli.Core;
using NBomber.Contracts;
using NBomber.CSharp;

namespace LoadTestCli.CliCommands
{
    internal class PiMixCommand : TestCommandBase
    {
        //private readonly string _piBaseUrl;
        private readonly HttpScenarioFactory _scenarioFactory;

        protected bool UseSync {get; set;}

        public override string Description => "Calls pi/piSleep and pi/piDigits a few times";

        public PiMixCommand(string name, string piBaseUrl, bool useSync) : base(name)
        {
            //_piBaseUrl = piBaseUrl;
            _scenarioFactory = new HttpScenarioFactory(piBaseUrl);
            UseSync = useSync;
        }

        public override bool TryParseInput(IList<string> args, out IList<string> remainingArgs)
        {
            if (!base.TryParseInput(args, out var remainingArgs1))
            {
                remainingArgs = remainingArgs1;
                return false;
            }

            //UrlToCall = ParseNextParamAsString(remainingArgs1, out var remainingArgs2) ?? UrlToCall;

            //if (string.IsNullOrWhiteSpace(UrlToCall))
            //{
            //    throw new ArgumentException("No url to call was specified!");
            //}

            //remainingArgs = remainingArgs2;
            remainingArgs = remainingArgs1;

            return true;
        }

        public override Scenario BuildTestScenario()
        {
            _scenarioFactory.Init();

            var steps = UseSync ? CreateSyncMixSteps() : CreateAsyncMixSteps();

            if (PostDelayInMs > 0)
            {
                steps.Add(_scenarioFactory.CreatePostDelayStep(PostDelayInMs));
            }

            var scenario = ScenarioBuilder.CreateScenario(Name, steps.ToArray());

            return _scenarioFactory.ConfigureRunSettings(scenario, TestRunnerSettings);
        }

        private List<IStep> CreateAsyncMixSteps()
        {
            return new List<IStep>
            {
                NewPiStep("piSleep1", "pi/piSleep/101/500"), 
                NewPiStep("pi1", "pi/piDigits/101"), 
                NewPiStep("piSleep2", "pi/piSleep/102/500"), 
                NewPiStep("pi2", "pi/piDigits/102")
            };
        }

        private IStep NewPiStep(string stepName, string relativePath)
        {
            return _scenarioFactory.CreateRelativeHttpGetStep(stepName, relativePath);
        }

        private List<IStep> CreateSyncMixSteps()
        {
            return new List<IStep>
            {
                NewPiStep("piSleepSync1", "pi/piSleepSync/101/500"),
                NewPiStep("piSync1", "pi/piDigits/101"),
                NewPiStep("piSleepSync2", "pi/piSleepSync/102/500"),
                NewPiStep("piSync2", "pi/piDigitsSync/102"),
            };
         }

        public override void ShowConfig(Action<string> writeOutputAction)
        {
            writeOutputAction("Scenario specific config:");
            ShowPostDelayConfig(writeOutputAction);
            ShowConfigValue(writeOutputAction, "useSync", UseSync ? 1 : 0);
            writeOutputAction("");
        }
    }
}
