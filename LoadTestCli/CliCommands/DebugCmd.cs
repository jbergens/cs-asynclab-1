﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NBomber.Contracts;

namespace LoadTestCli.CliCommands
{
    internal class DebugCmd : TestCommandBase
    {
        private int _sleepTimeInMs = 100;

        public override string Description => "Will just show given parameters ans sleep a short while.";

        public DebugCmd(string name) : base(name)
        {
        }

        public override Scenario BuildTestScenario()
        {
            var step = ScenarioFactory.CreateSleepStep(_sleepTimeInMs, "debugSleep");            
            var scenario = CreateWrappedScenario("DebugScenario", step);

            // And configure the scenario to get default values (some may be overwritten by config later)
            //
            return ConfigureScenario(scenario, TestRunnerSettings);
        }

        public override bool TryParseInput(IList<string> args, out IList<string> remainingArgs)
        {
            bool result = base.TryParseInput(args, out var remainingArgs1);

            _sleepTimeInMs = ParseIntegerParam("s", remainingArgs1, out remainingArgs) ?? 100;

            return result;
        }

        public override void ShowCommandUsage(Action<string> writeOutputAction, string scriptName)
        {
            writeOutputAction($"Usage: {scriptName} {Name} {CommonParamsUsageText} -s (sleepTime)");
            writeOutputAction(Description);
            ShowParamDescription(writeOutputAction, "-s", "Sets time in ms to sleep for each call");
            ShowCommonParamsDescription(writeOutputAction);
        }

        public override void ShowConfig(Action<string> writeOutputAction)
        {
            base.ShowConfig(writeOutputAction);
            ShowParamValue(writeOutputAction, "sleepTime", _sleepTimeInMs.ToString());
        }
    }
}