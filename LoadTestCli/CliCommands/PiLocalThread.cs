﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AsyncLab1.Core;
using Microsoft.AspNetCore.Mvc;
using NBomber.Contracts;
using NBomber.CSharp;

namespace LoadTestCli.CliCommands
{
    internal class PiLocalThread : TestCommandBase
    {
        private int _sleepTimeInMs;
        private int _piDigits;

        public override string Description => "Will calculate pi using multiple local threads";

        public PiLocalThread(string name, int piDigits) : base(name)
        {
            _piDigits = piDigits;
        }

        public override Scenario BuildTestScenario()
        {
            var step = CreatePiThreadCalcStep("piThread1", _piDigits);
            var scenario = CreateWrappedScenario("DebugScenario", step);

            // And configure the scenario to get default values (some may be overwritten by config later)
            //
            return ConfigureScenario(scenario, TestRunnerSettings);
        }

        private IStep CreatePiThreadCalcStep(in string name, in int digits)
        {
            int myDigits = digits;
            
            var step = Step.Create(name, async context =>
            {
                // Force creation of some threads and then wait for them. Purpose is to see what happens when we have many threads
                Thread thread1 = new Thread(CalcPiDigits);
                Thread thread2 = new Thread(CalcPiDigits);
                Thread thread3 = new Thread(CalcPiDigits);

                thread1.Start(myDigits);
                thread2.Start(myDigits);
                thread3.Start(myDigits);

                // wait for threads
                await Task.Run(() =>
                {
                    // Should use a ThreadPool thread until they run out
                    thread1.Join(TimeSpan.FromSeconds(120));
                    thread2.Join(TimeSpan.FromSeconds(120));
                    thread3.Join(TimeSpan.FromSeconds(120));
                });
                
                return Response.Ok();
            });

            return step;
        }

        private void CalcPiDigits(object threadInput)
        {
            int digits = (int) threadInput;

            var calculator = new PiCalculator();

            calculator.Calc(digits);
            Thread.Sleep(_sleepTimeInMs);
        }

        public override bool TryParseInput(IList<string> args, out IList<string> remainingArgs)
        {
            bool result = base.TryParseInput(args, out var remainingArgs1);

            _sleepTimeInMs = ParseIntegerParam("s", remainingArgs1, out remainingArgs) ?? 50;

            return result;
        }

        public override void ShowCommandUsage(Action<string> writeOutputAction, string scriptName)
        {
            writeOutputAction($"Usage: {scriptName} {Name} {CommonParamsUsageText} -s (sleepTime)");
            writeOutputAction(Description);
            ShowParamDescription(writeOutputAction, "-s", "Sets time in ms to sleep for each call");
            ShowCommonParamsDescription(writeOutputAction);
        }

        public override void ShowConfig(Action<string> writeOutputAction)
        {
            base.ShowConfig(writeOutputAction);
            ShowParamValue(writeOutputAction, "sleepTime", _sleepTimeInMs.ToString());
        }

    }
}