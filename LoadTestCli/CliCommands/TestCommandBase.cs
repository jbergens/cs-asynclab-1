﻿using System;
using System.Collections.Generic;
using System.Linq;
using NBomber.Contracts;
using NBomber.CSharp;
using LoadTestCli.Core;

namespace LoadTestCli.CliCommands
{
    internal abstract class TestCommandBase : ICliCommand, ILoadTestCommand
    {
        private const int DefaultPostDelayInMs = 500;

        protected int PostDelayInMs = DefaultPostDelayInMs;
        protected TestRunnerSettings TestRunnerSettings = new TestRunnerSettings(2, 30, 5);
        protected ScenarioFactoryBase ScenarioFactory = new ScenarioFactoryBase();

        public abstract string Description { get; }
        public string Name { get; }
        public bool RunNonInteractive { get; protected set; }

        protected TestCommandBase(string name)
        {
            Name = name;
        }

        public virtual bool TryParseInput(IList<string> args, out IList<string> remainingArgs)
        {
            ParseCommonParams(args, out remainingArgs);

            return true;
        }

        protected void ParseCommonParams(IList<string> args, out IList<string> remainingArgs)
        {
            if (args == null || args.Count == 0)
            {
                remainingArgs = args;
                return;
            }

            IList<string> remainingArgs1, remainingArgs2;

            ParseConcurrentRunners(args, out remainingArgs1);
            ParseDuration(remainingArgs1, out remainingArgs2);
            ParseWarmup(remainingArgs2, out remainingArgs1);
            ParsePostDelayInMs(remainingArgs1, DefaultPostDelayInMs, out remainingArgs2);
            
            remainingArgs = remainingArgs2;
        }

        private void ParseConcurrentRunners(IList<string> args, out IList<string> remainingArgs)
        {
            TestRunnerSettings.ConcurrentRunners = ParseIntegerParam("r", args, out remainingArgs) ?? TestRunnerSettings.ConcurrentRunners;
        }

        protected int? ParseIntegerParam(string name, IList<string> args, out IList<string> remaining)
        {
            string code = "-" + name;
            int idx = args.IndexOf(code);

            if (idx <= -1)
            {
                remaining = args;
                return null;
            }

            if (args.Count <= (idx + 1))
            {
                throw new Exception($"Missing argument after '{code}'! Should be an integer.");
            }

            int? result = ParseNullableInteger(args[idx + 1], code);

            remaining = Remove2Items(args, idx);

            return result;
        }

        protected int? ParseNullableInteger(string value, string argName = "argument")
        {
            try
            {
                return string.IsNullOrWhiteSpace(value) ? (int?)null : int.Parse(value);
            }
            catch (Exception)
            {
                throw new Exception($"Could not parse argument to '{argName}'!\r\nShould be an integer, but was '{value}'.");
            }
        }

        private static IList<string> Remove2Items(IList<string> args, int idx)
        {
            var remaining = new List<string>(args);
            
            remaining.RemoveAt(idx);
            remaining.RemoveAt(idx);
            
            return remaining;
        }

        private void ParseDuration(IList<string> args, out IList<string> remainingArgs)
        {
            TestRunnerSettings.DurationInSec = ParseIntegerParam("d", args, out remainingArgs) ?? TestRunnerSettings.DurationInSec;
        }

        private void ParseWarmup(IList<string> args, out IList<string> remainingArgs)
        {
            TestRunnerSettings.WarmupDurationInSec = ParseIntegerParam("w", args, out remainingArgs) ?? TestRunnerSettings.WarmupDurationInSec;
        }

        protected void ParsePostDelayInMs(IList<string> args, int defaultValue, int minPostDelayInMs, out IList<string> remainingArgs)
        {
            ParsePostDelayInMs(args, defaultValue, out remainingArgs);
            EnsurePostDelayIsAtLeast(minPostDelayInMs);
        }

        protected void ParsePostDelayInMs(IList<string> args, int defaultValue, out IList<string> remainingArgs)
        {
            PostDelayInMs = ParseIntegerParam("p", args, out remainingArgs) ?? defaultValue;
        }

        protected void OldParsePostDelayInMs(IList<string> args, int defaultValue, out IList<string> remainingArgs)
        {
            PostDelayInMs = ParseNextParamAsInteger(args, "postDelayInMs", out remainingArgs) ?? defaultValue;
        }

        protected int? ParseNextParamAsInteger(IList<string> args, string paramName, out IList<string> remainingArgs)
        {
            int? result = ParseNullableInteger(args.FirstOrDefault(), paramName);

            remainingArgs = (result == null) ? args : Tail(args);

            return result;
        }

        private IList<T> Tail<T>(IList<T> list)
        {
            if (list == null || list.Count == 0)
            {
                return list;
            }

            var remaining = new List<T>(list);
            remaining.RemoveAt(0);

            return remaining;
        }

        protected string ParseNextParamAsString(IList<string> args, out IList<string> remainingArgs)
        {
            remainingArgs = Tail(args);

            return args.FirstOrDefault();
        }

        public virtual void ShowCommandUsage(Action<string> writeOutputAction, string scriptName)
        {
            ShowStandardCommandUsage(writeOutputAction, scriptName);
        }

        private void ShowStandardCommandUsage(Action<string> writeOutputAction, string scriptName)
        {
            writeOutputAction($"Usage: {scriptName} {Name} {CommonParamsUsageText}");
            writeOutputAction(Description);
            ShowCommonParamsDescription(writeOutputAction);
        }

        public string CommonParamsUsageText => "-r (runners) -d (duration) -p (postDelay) -w (warmupDuration)";

        protected void ShowCommonParamsDescription(Action<string> writeOutputAction)
        {
            ShowParamDescription(writeOutputAction, "-r", "Number of runners (threads) to use while running the test");
            ShowParamDescription(writeOutputAction, "-d", "Test duration in seconds");
            ShowParamDescription(writeOutputAction, "-w", "Warmup duration in seconds");
            ShowParamDescription(writeOutputAction, "-p", "Delay in ms to add after each scenario");
        }

        protected void ShowParamDescription(Action<string> writeOutputAction, string name, string text)
        {
            writeOutputAction($"\t{name}:\t\t{text}");
        }

        protected void ShowParamValue(Action<string> writeOutputAction, string name, string value)
        {
            writeOutputAction($"\t{name}:\t\t{value}");
        }

        public abstract Scenario BuildTestScenario();

        public virtual void ShowConfig(Action<string> writeOutputAction)
        {
            ShowParamValue(writeOutputAction, "postDelay", PostDelayInMs.ToString());
        }

        protected void ShowConfigValue(Action<string> writeOutputAction, string name, in int value)
        {
            writeOutputAction($"\t{name}: {value}");
        }

        protected void ShowConfigValue(Action<string> writeOutputAction, string name, in bool value)
        {
            writeOutputAction($"\t{name}: {value}");
        }

        protected void ShowPostDelayConfig(Action<string> writeOutputAction)
        {
            ShowConfigValue(writeOutputAction, "PostDelayInMs", PostDelayInMs);
        }

        protected void EnsurePostDelayIsAtLeast(int minDelayInMs)
        {
            if (PostDelayInMs < minDelayInMs)
            {
                throw new ArgumentOutOfRangeException(nameof(PostDelayInMs), $"postDelayInMs must be more than {minDelayInMs}ms for test {Name}!");
            }
        }

        protected virtual Scenario ConfigureScenario(Scenario scenario, TestRunnerSettings testRunnerSettings)
        {
            return ScenarioFactory.ConfigureRunSettings(scenario, testRunnerSettings);
        }

        protected Scenario CreateWrappedScenario(string name, IStep singleStep)
        {
            var list= new List<IStep>{singleStep};
            
            return CreateWrappedScenario(name, list);
        }

        protected Scenario CreateWrappedScenario(string name, IList<IStep> steps)
        {
            if (steps == null || steps.Count == 0)
            {
                throw new ArgumentOutOfRangeException(nameof(steps), "Steps must be a list with at least on step!");
            }

            if (PostDelayInMs > 0)
            {
                steps.Add(ScenarioFactory.CreateStaticDelayStep(name, PostDelayInMs));
            }

            return ScenarioBuilder.CreateScenario(name, steps.ToArray());
        }

        private void DbgShowArgs(string intro, IList<string> args)
        {
            string values = string.Join(',', args);

            Console.WriteLine($"{intro}: code ({args.Count}) [{values}]");
        }
    }
}