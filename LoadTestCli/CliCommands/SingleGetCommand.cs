﻿using System;
using System.Collections.Generic;
using LoadTestCli.Core;
using NBomber.Contracts;

namespace LoadTestCli.CliCommands
{
    internal class SingleGetCommand : TestCommandBase
    {
        private readonly HttpScenarioFactory _scenarioFactory;

        public string UrlToCall { get; set; }
        public override string Description => "Calls " + (UrlToCall ?? "specified url");

        public SingleGetCommand(string name, string fullUrl) : base(name)
        {
            _scenarioFactory = new HttpScenarioFactory();
            UrlToCall = fullUrl; // fullUrl may be null, must be set later in that case
            PostDelayInMs = 200; // Default value, can be overwritten by args or config
        }

        public override bool TryParseInput(IList<string> args, out IList<string> remainingArgs)
        {
            if (!base.TryParseInput(args, out var remainingArgs1))
            {
                remainingArgs = remainingArgs1;
                return false;
            }

            UrlToCall = ParseNextParamAsString(remainingArgs1, out var remainingArgs2) ?? UrlToCall;

            if (string.IsNullOrWhiteSpace(UrlToCall))
            {
                throw new ArgumentException("No url to call was specified!");
            }

            remainingArgs = remainingArgs2;
            //ParseQliroTestConfig(Name + ".test.config");

            return true;
        }

        public override Scenario BuildTestScenario()
        {
            _scenarioFactory.Init();

            var scenario = _scenarioFactory.CreateSingleAbsoluteGetScenario(Name, UrlToCall, PostDelayInMs);

            return _scenarioFactory.ConfigureRunSettings(scenario, TestRunnerSettings);
        }

        public override void ShowConfig(Action<string> writeOutputAction)
        {
            writeOutputAction("Scenario specific config:");
            ShowPostDelayConfig(writeOutputAction);
            writeOutputAction("");
        }
    }
}