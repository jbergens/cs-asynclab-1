﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NBomber.Contracts;
using NBomber.CSharp;

namespace LoadTestCli.CliCommands
{
    internal class HelloCommand : TestCommandBase
    {
        private int _sleepTimeInMs = 100;

        public override string Description => "Will just sleep a short while, showing that the test framework works.";

        public HelloCommand(string name) : base(name)
        {
        }

        public override Scenario BuildTestScenario()
        {
            // first, you need to create a step
            var step = Step.Create("step-1", async context =>
            {
                context.Data = new object();
                // you can do any logic here: go to http, websocket etc
                await Task.Delay(TimeSpan.FromMilliseconds(_sleepTimeInMs));
                
                return Response.Ok(); // good idea to return ok
            });

            // after creating a step you should create a scenario using the step.
            // CreateWrappedScenario() will also add some standard things like PostDelay if necessary args are provided.
            //
            var scenario = CreateWrappedScenario("HelloWorld", step);

            // And configure the scenario to get default values (some may be overwritten by config later)
            //
            return ConfigureScenario(scenario, TestRunnerSettings);
        }

        public override bool TryParseInput(IList<string> args, out IList<string> remainingArgs)
        {
            // This method is only needed if the command has special arguments to parse
            bool result = base.TryParseInput(args, out var remainingArgs1);

            _sleepTimeInMs = ParseIntegerParam("s", remainingArgs1, out remainingArgs) ?? 100;

            return result;
        }

        public override void ShowCommandUsage(Action<string> writeOutputAction, string scriptName)
        {
            writeOutputAction($"Usage: {scriptName} {Name} {CommonParamsUsageText} -s (sleepTime)");
            writeOutputAction(Description);
            ShowParamDescription(writeOutputAction, "-s", "Sets time in ms to sleep for each call");
            ShowCommonParamsDescription(writeOutputAction);

            // Other commands may want to use this form instead
            //
            //writeOutputAction($"Usage: {scriptName} {Name} {CommonParamsUsageText}");
            //writeOutputAction(Description);
            // ShowCommonParamsDescription(writeOutputAction)
        }

        public override void ShowConfig(Action<string> writeOutputAction)
        {
            // This method is only needed if the command has special arguments to parse
            ShowParamValue(writeOutputAction, "sleepTime", _sleepTimeInMs.ToString());
        }
    }
}