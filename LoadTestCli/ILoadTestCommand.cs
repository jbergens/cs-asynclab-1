﻿using System;
using NBomber.Contracts;

namespace LoadTestCli
{
    internal interface ILoadTestCommand
    {
        string Name { get; }
        bool RunNonInteractive { get; }

        Scenario BuildTestScenario();
        void ShowConfig(Action<string> writeOutputAction);
    }
}