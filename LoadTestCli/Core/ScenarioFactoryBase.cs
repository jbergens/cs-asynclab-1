﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NBomber.Contracts;
using NBomber.CSharp;

namespace LoadTestCli.Core
{
    internal class ScenarioFactoryBase
    {
        public IStep CreateSleepStep(int sleepTimeInMs, string name = "sleep")
        {
            return Step.Create(name, async context =>
            {
                await Task.Delay(TimeSpan.FromMilliseconds(sleepTimeInMs));
                
                return Response.Ok();
            });
        }

        public IStep CreateStaticDelayStep(string stepName, int delayInMs)
        {
            if (delayInMs <= 0) return null;

            string name = stepName + $"_delay_{delayInMs}ms";

            return CreateSleepStep(delayInMs, name);
        }

        public Scenario ConfigureRunSettings(Scenario scenario, TestRunnerSettings runnerSettings)
        {
            var scenario1 = scenario.WithConcurrentCopies(runnerSettings.ConcurrentRunners)
                .WithDuration(TimeSpan.FromSeconds(runnerSettings.DurationInSec));

            // Must overwrite earlier values even if the new value is 0
            return scenario1.WithWarmUpDuration(TimeSpan.FromSeconds(runnerSettings.WarmupDurationInSec));

            //return runnerSettings.WarmupDurationInSec <= 0 ? 
            //    scenario1 :
            //    scenario1.WithWarmUpDuration(TimeSpan.FromSeconds(runnerSettings.WarmupDurationInSec));
        }

        public virtual void Init()
        {
            //_httpClientWrapper = new HttpClientWrapper();
            //_httpRequestHeaders = BuildHttpRequestHeaders();
            //_httpClient = GetHttpClientWithTimeoutSet(_httpRequestHeaders);
        }

        public List<IStep> AsStepList(IStep step1, IStep step2 = null, IStep step3 = null)
        {
            var steps = new List<IStep> {step1};
            
            if (step2 != null)
            {
                steps.Add(step2);
            }
            if (step3 != null)
            {
                steps.Add(step3);
            }

            return steps;
        }
    }
}