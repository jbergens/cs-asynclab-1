﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using NBomber.Contracts;
using NBomber.CSharp;
using NBomber.Http.CSharp;

namespace LoadTestCli.Core
{
    internal class HttpScenarioFactory : ScenarioFactoryBase
    {
        private const double HttpTimeoutInSec = 60;

        private HttpClient _httpClient;
        private Dictionary<string, string> _httpRequestHeaders;
        protected string _baseUrl;
        private readonly Uri _baseUri;

        public HttpScenarioFactory()
        {
        }

        public HttpScenarioFactory(string baseUrl)
        {
            _baseUrl = baseUrl;
            _baseUri = new Uri(_baseUrl);
        }

        public override void Init()
        {
            base.Init();
        }

        private Dictionary<string, string> BuildHttpRequestHeaders()
        {
            return new Dictionary<string, string>();
        }

        public IStep CreateRelativeHttpGetStep(string stepName, string relativePath)
        {
            if (_baseUrl == null) throw new Exception("HttpScenarioFactory cannot create relative GET since baseUrl is not set!");

            var uri = new Uri(_baseUri, relativePath);

            return CreateAbsoluteHttpGetStep(stepName, uri.AbsoluteUri);
        }

        public IStep CreateAbsoluteHttpGetStep(string stepName, string url)
        {
            Console.WriteLine($"[Step '{stepName}'] Creating GET for url: {url}\r\n");

            // Syntax from NBomber examples, async seems to be needed even though VS or Resharper warns about it

            return HttpStep.Create(stepName, async (context) =>
                Http.CreateRequest("GET", url)
                    .WithHeader("Accept", "application/json,application/xml,text/html,text/plain"));
        }

        private IStep CreateHttpPostStep(string stepName, string url, StringContent content)
        {
            return HttpStep.Create(stepName, async (context) =>
                    Http.CreateRequest("POST", url)
                        .WithHeader("Accept", "application/json,application/xml")
                //.WithHeader("Authorization", BearerContent)
                //.WithHeader("Cookie", "cookie1=value1; cookie2=value2") // Example of using a cookie
            );
        }

        private async Task<HttpResponseMessage> PostAsync(string requestUrl, HttpContent content)
        {
            var response = await _httpClient.PostAsync(requestUrl, content);

            return response;
        }

        private HttpClient GetHttpClientWithTimeoutSet(Dictionary<string, string> httpRequestHeaders)
        {
            //var httpClient = _httpClientFactory.CreateClient();
            var httpClient = new HttpClient();

            if (httpRequestHeaders != null)
            {
                foreach (var httpRequestHeader in httpRequestHeaders)
                {
                    httpClient.DefaultRequestHeaders.Add(httpRequestHeader.Key, httpRequestHeader.Value);
                }
            }

            httpClient.Timeout = TimeSpan.FromSeconds(HttpTimeoutInSec);

            return httpClient;
        }

        public Scenario CreateSingleAbsoluteGetScenario(string scenarioName, string absoluteUrl, int postDelayInMs = 0, string stepName = "httpGet")
        {
            var step1 = CreateAbsoluteHttpGetStep(stepName, absoluteUrl);
            var delayStep = CreatePostDelayStep(postDelayInMs);
            var steps = AsStepList(step1, delayStep);

            return ScenarioBuilder.CreateScenario(scenarioName, steps.ToArray());
        }

        public Scenario CreateSingleGetScenario(string scenarioName, string path, int postDelayInMs = 0, string stepName = "httpGet")
        {
            var step1 = CreateRelativeHttpGetStep(stepName, path);
            var delayStep = CreatePostDelayStep(postDelayInMs);
            var steps = AsStepList(step1, delayStep);

            return ScenarioBuilder.CreateScenario(scenarioName, steps.ToArray());
        }

        public IStep CreatePostDelayStep(in int postDelayInMs)
        {
            return CreateStaticDelayStep("postDelay", postDelayInMs);
        }
    }
}