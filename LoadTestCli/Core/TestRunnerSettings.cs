﻿namespace LoadTestCli.Core
{
    internal class TestRunnerSettings
    {
        public int ConcurrentRunners { get; set; }
        public int DurationInSec  { get; set; }
        public int WarmupDurationInSec  { get; set; }

        public TestRunnerSettings()
        {
            ConcurrentRunners = 2;
            DurationInSec = 30;
            WarmupDurationInSec = 5;
        }

        public TestRunnerSettings(int concurrentRunners, int durationInSec, int warmupDurationInSec = 5)
        {
            ConcurrentRunners = concurrentRunners;
            DurationInSec = durationInSec;
            WarmupDurationInSec = warmupDurationInSec;
        }
    }
}