﻿using NBomber.Http;
using NBomber.Http.CSharp;

namespace QuickLoadTester.Core
{
    public static class HttpRequestExtensions
    {
        public static HttpRequest WithApiAcceptHeader(this HttpRequest request)
        {
            return request.WithHeader("Accept", "application/json,application/xml");
        }
        public static HttpRequest WithCommonAcceptHeader(this HttpRequest request)
        {
            return request.WithHeader("Accept", "application/json,application/xml,text/html,text/plain");
        }
    }
}
