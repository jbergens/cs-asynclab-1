﻿using System;
using System.Collections.Generic;

namespace LoadTestCli
{
    internal interface ICliCommand
    {
        string Description { get; }
        string Name { get; }

        bool TryParseInput(IList<string> args, out IList<string> remainingArgs);
        void ShowCommandUsage(Action<string> writeOutputAction, string scriptName);
        //void ShowCurrentConfig(Action<string> writeOutputAction);
    }
}