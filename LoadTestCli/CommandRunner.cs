﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LoadTestCli
{
    internal class CommandRunner
    {
        private readonly string _scriptName;

        public CommandRunner(string scriptName)
        {
            _scriptName = scriptName;
        }

        public ICliCommand ChooseCommand(Dictionary<string, ICliCommand> commands, string[] args)
        {
            ICliCommand command;

            if (args.Length < 1) { return null; }

            string name = args[0].ToLower();

            if (!commands.TryGetValue(name, out command) && !WantsDetailedHelp(args))
            {
                Console.WriteLine($"Unknown command: '{name}'!");
            }

            return command;
        }

        public List<string> RemainingArgs(string[] args)
        {
            return args.Length == 0 ? new List<string>() : args.Skip(1).ToList();
        }


        public bool NeedsHelp(string[] args, ICliCommand command)
        {
            return command == null || WantsDetailedHelp(args);
        }

        private static bool WantsDetailedHelp(string[] args)
        {
            return args.Any(a => a.ToLower() == "-h");
        }

        public void ShowHelp(string[] args, Dictionary<string, ICliCommand> allCommands, ICliCommand chosenCommand)
        {
            if (WantsDetailedHelp(args))
            {
                ShowDetailedHelp(allCommands, chosenCommand);
            }
            else
            {
                var names = String.Join('|', allCommands.Select(c => c.Key));

                Console.WriteLine($"Usage: {_scriptName} ({names})");
            }
        }

        private void ShowDetailedHelp(Dictionary<string, ICliCommand> allCommands, ICliCommand chosenCommand)
        {
            if (chosenCommand != null)
            {
                chosenCommand.ShowCommandUsage(Console.WriteLine, _scriptName);
                return;
            }

            Console.WriteLine($"Usage: {_scriptName} (test)");
            Console.WriteLine("Where (test) is any of the following:");

            foreach (var pair in allCommands)
            {
                var command = pair.Value;
                Console.WriteLine($"{pair.Key}\t\t {command.Description}");
            }
        }

        public TCommand CastCommandType<TCommand>(ICliCommand command) where TCommand : class
        {
            if (command == null) throw new ArgumentNullException(nameof(command));

            TCommand result = command as TCommand;

            if (result == null) throw new ArgumentOutOfRangeException("The chosen command does not implement " + typeof(TCommand));

            return result;
        }
    }
}