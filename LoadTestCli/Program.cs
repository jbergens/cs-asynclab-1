﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using NBomber.Contracts;
using NBomber.CSharp;
using LoadTestCli.CliCommands;
using NBomber.Configuration;

namespace LoadTestCli
{
    [SuppressMessage("ReSharper", "StringLiteralTypo")]
    class Program
    {
        private const string ScriptName = "LoadTestCli";
        //private const string ForecastBaseUrl = "https://localhost:44399/"; // TODO Move this to config
        //private const string PiBaseUrl = "https://localhost:44399/"; // TODO Move this to config
        private const string ForecastBaseUrl = "http://localhost:5002/"; // TODO Move this to config
        private const string PiBaseUrl = "http://localhost:5002/"; // TODO Move this to config

        static void Main(string[] args)
        {
            var allCommands = GetCommandList();
            var cmdRunner = new CommandRunner(ScriptName);
            var command = cmdRunner.ChooseCommand(allCommands, args);

            if (command == null || cmdRunner.NeedsHelp(args, command))
            {
                cmdRunner.ShowHelp(args, allCommands, command);
                return;
            }

            Console.WriteLine($"Before TryParseInput: args ({args.Length}): " + string.Join(',', args));

            if (!TryParseInput(args, cmdRunner, command))
            {
                Console.WriteLine("");
                command.ShowCommandUsage(Console.WriteLine, ScriptName);
                return;
            }

            // ReSharper disable once SuspiciousTypeConversion.Global
            var testCommand = cmdRunner.CastCommandType<ILoadTestCommand>(command);
            var context = BuildLoadTest(testCommand);

            RunLoadTest(testCommand, context);
        }

        private static bool TryParseInput(string[] args, CommandRunner cmdRunner, ICliCommand command)
        {
            try
            {
                var actualArgs = cmdRunner.RemainingArgs(args);
                bool parsedOk = command.TryParseInput(actualArgs, out var remainingArgs); // Ignoring remainingArgs

                if (remainingArgs.Count != 0)
                {
                    Console.WriteLine("Could not parse input!\r\nToo many arguments was given.");
                }

                return parsedOk && remainingArgs.Count == 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Could not parse input!\r\nError: " + ex.Message);
                Console.WriteLine("\r\nStacktrace:" + ex.StackTrace);
                return false;
            }
        }

        private static Dictionary<string, ICliCommand> GetCommandList()
        {
            return new Dictionary<string, ICliCommand>
            {
                {"hello", new HelloCommand("hello") },
                {"debug", new DebugCmd("debug") },

                {"fakeforecast", CreateForecastCmd("fakeforecast", "weatherforecast")},
                {"shortforecast", CreateForecastCmd("shortForecast", "weatherforecast/short")},
                {"shortforecastsync", CreateForecastCmd("shortForecastsync", "weatherforecast/shortsync")},
                {"longforecast", CreateForecastCmd("longForecast", "weatherforecast/long")},
                {"longforecastsync", CreateForecastCmd("longForecastsync", "weatherforecast/longsync")},
                {"forecastsleep", CreateForecastCmd("forecastsleep", "weatherforecast/shortsleep/500")},
                {"forecastsleepsync", CreateForecastCmd("forecastsleepsync", "weatherforecast/shortsleepsync/500")},
                {"helloforecast", CreateForecastCmd("helloForecast", "weatherforecast/hello")},
                {"google", new SingleGetCommand("google", "https://www.google.se/")},
                {"httpget", new SingleGetCommand("httpget", null)}, // Set fullUrl with args later

                {"pi1k", CreatePiCmd("pi1k", "pi/pi1k")},
                {"pi1ksync", CreatePiCmd("pi1ksync", "pi/pi1ksync")},
                {"pi1kstatic", CreatePiCmd("pi1kstatic", "pi/pi1kStatic")},
                {"pi10kstatic", CreatePiCmd("pi10kstatic", "pi/pi10kStatic")},
                {"pi10k", CreatePiCmd("pi10k", "pi/piDigits/10000")},
                {"pi10ksync", CreatePiCmd("pi10ksync", "pi/piDigitsSync/10000")},
                {"pi30k", CreatePiCmd("pi10k", "pi/piDigits/30000")},
                {"pi30ksync", CreatePiCmd("pi10ksync", "pi/piDigitsSync/30000")},
                {"pi1ksleep", CreatePiCmd("pi1ksleep", "pi/piSleep/1000/1000")},
                {"pi1ksleepsync", CreatePiCmd("pi1ksleepsync", "pi/piSleepSync/1000/1000")},
                {"pi10ksleep", CreatePiCmd("pi10ksleep", "pi/piSleep/10000/1000")},
                {"pi10ksleepsync", CreatePiCmd("pi10ksleepsync", "pi/piSleepSync/10000/1000")},
                {"pimix", new PiMixCommand("pimix", PiBaseUrl, false)},
                {"pimixsync", new PiMixCommand("pimixsync", PiBaseUrl, true)},
                {"pilocalthread", new PiLocalThread("pilocalthread", 1000)},
                {"pilocalthread10k", new PiLocalThread("pilocalthread", 10000)},
            };
        }

        private static ICliCommand CreateForecastCmd(string name, string path)
        {
            return new SingleGetCommand(name, ForecastBaseUrl + path);
        }

        private static ICliCommand CreatePiCmd(string name, string path)
        {
            return new SingleGetCommand(name, PiBaseUrl + path);
        }

        private static TestContext BuildLoadTest(ILoadTestCommand command)
        {
            Console.WriteLine($"Preparing test '{command.Name}'..");

            var scenario = command.BuildTestScenario();
            var context = NBomberRunner.RegisterScenarios(scenario);

            var newContext = ConfigureTestContext(command.Name, context);

            Console.WriteLine("\r\nScenario config");
            command.ShowConfig(Console.WriteLine);
            ShowScenarioConfig(scenario, context, newContext);

            return context;
        }

        private static TestContext ConfigureTestContext(string name, TestContext context)
        {
            string configFileName = name + ".config.json"; // TODO: Get config file name from command line

            //Console.WriteLine($"Looking for config '{configFileName}'..");
            if (File.Exists(configFileName))
            {
                Console.WriteLine("Not using any config"); // Config files stopped working with v0.16
                //return LoadConfig(context, configFileName);
            }

            return context;
        }

        private static TestContext LoadConfig(TestContext context, string configFileName)
        {
            var newContext =  context.LoadTestConfig(configFileName);

            // ToString() gave better looking output than serializing to json
            Console.WriteLine("\r\nConfig after load: " + newContext.TestConfig.ToString());

            return newContext;
        }

        private static void ShowScenarioConfig(Scenario scenario, TestContext originalContext, TestContext newContext)
        {
            //if (originalContext == newContext)
            //{
            //    Console.WriteLine("No config file found. Using scenario default values or command line arguments.");
            //}
            ShowScenarioConfig(scenario);
        }

        private static void ShowScenarioConfig(Scenario scenario)
        {
            Console.WriteLine("Runtime config");
            Console.WriteLine("\tConcurrentCopies: " + scenario.ConcurrentCopies);
            Console.WriteLine("\tDuration: " + scenario.Duration.ToString("g"));
            Console.WriteLine("\tWarmUpDuration: " + scenario.WarmUpDuration.ToString("g"));
        }

        private static void RunLoadTest(ILoadTestCommand command, TestContext context)
        {
            if (command.RunNonInteractive)
            {
                Console.WriteLine("\r\nRunning test in non-interactive mode");
            }
            else
            {
                Console.WriteLine($"\r\nPush any key to start test '{command.Name}'..");
                Console.ReadKey();
            }

            context.RunInConsole();
        }
    }
}