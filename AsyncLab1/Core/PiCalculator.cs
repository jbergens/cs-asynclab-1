﻿using System.Diagnostics;
using System.Numerics;

namespace AsyncLab1.Core
{
    public class PiCalculator {
        private const int _precisionFix = 20;
        private Stopwatch _watch;
        private readonly BigInteger _tenPowTwenty = BigInteger.Pow(10, _precisionFix);

        public PiCalculator() {}

        public BigInteger Calc(int digits)
        {
            BigInteger pi = CalculatePi(digits);

            return pi;
        }

        public int DoneIterations { get; set; }

        private BigInteger CalculatePi(int digits) {
            _watch = Stopwatch.StartNew();

            //_x = 3m * (10m ** (BigInt(digits) + 20m)); // js code
            BigInteger x = 3 * BigInteger.Pow(10, digits + _precisionFix); // A really large number starting with 3
            var piData = new PiData(x, x);

            while (piData.X > 0)
            {
                CalcPartOfPi(piData);
            }

            _watch.Stop();
            
            // After the last calculation, show in decimal
            //BigInteger pi2 = piData.Pi / 10_000_000_000 / 10_000_000_000; // It should equal to divide by 10^20.
            BigInteger pi2 = piData.Pi / _tenPowTwenty; // Will shave off last digits to avoid errors
            DoneIterations = (piData.I - 1) / 2;

            return pi2;
        }

        private void CalcPartOfPi(PiData piData)
        {
            int i = piData.I; // Copying to local variables to try to enchance perf
            BigInteger x = piData.X;
            BigInteger pi = piData.Pi;
            
            if (x > 0) {
                // 100 is the granularity of the calculations between possible ui updates
                for (var j = 0; j < 100; ++j) { 
                    x = x * i / ((i + 1) * 4);
                    pi += x / (i + 2);
                    i += 2;
                }

                piData.X = x;
                piData.Pi = pi;
                piData.I = i;

                //string pistr16 = pi.ToString("X");
                
                //document.getElementById('terms').innerHTML = ((i - 1n) / 2n).toString();
                //document.getElementById('dcount').innerHTML = (((pistr16.length - x.toString(16).length) * 1.20412) - 20).toFixed(0);

                //document.getElementById('digits').innerHTML = pistr16.replace(/.{10}/g, "$& ");
                //document.getElementById('elapsed').innerHTML = ((new Date() - startTime)/1000).toFixed(3) + " s";

                //setTimeout(doSomeCalcs, 0);
            }
        }

        private class PiData
        {
            public PiData() {}

            public PiData(BigInteger pi, BigInteger x) {
                I = 1;
                Pi = pi;
                X = x;
            }

            public int I;
            public BigInteger Pi;
            public BigInteger X;
        }
     }
 }
