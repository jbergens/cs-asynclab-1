﻿using System;
using System.Linq;
using System.Numerics;
using System.Threading;
using System.Threading.Tasks;
using AsyncLab1.Core;
using Microsoft.AspNetCore.Mvc;

namespace AsyncLab1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PiController : ControllerBase
    {
        private readonly string _pi1k;
        private readonly string _pi10k;

        public PiController()
        {
            var calculator = new PiCalculator();

            _pi1k = calculator.Calc(1000).ToString();
            _pi10k = calculator.Calc(10000).ToString();
        }

        [HttpGet]
        public string Get()
        {
            var piCalculator = new PiCalculator();
            var pi = piCalculator.Calc(100);

            return pi.ToString();
        }

        [HttpGet("pi1k")]
        public async Task<IActionResult> GetPi1k()
        {
            string piStr = await CalcPiStr(1000);

            return Ok(piStr);
        }

        [HttpGet("pi1ksync")]
        public string GetPi1kSync()
        {
            return CalcPiStrSync(1000);
        }

        private async Task<string> CalcPiStr(int digits)
        {
            var piCalculator = new PiCalculator();

            return await Task.Run(() =>
            {
                BigInteger pi = piCalculator.Calc(digits);
                
                return pi.ToString();
            });
        }

        [HttpGet("pi1kstatic")]
        public IActionResult GetPi1kStatic()
        {
            return Ok(_pi1k);
        }

        [HttpGet("pi10kstatic")]
        public IActionResult GetPi10kStatic()
        {
            return Ok(_pi10k);
        }

        private string CalcPiStrSync(int digits)
        {
            var piCalculator = new PiCalculator();
            BigInteger pi = piCalculator.Calc(digits);
                
            return pi.ToString();
        }

        [HttpGet("piDigits/{digits}")]
        public async Task<IActionResult> GetPiForNDigits(int digits)
        {
            string piStr = await CalcPiStr(digits);

            return Ok(piStr);
            //return Ok(new { pi = piStr });
        }

        [HttpGet("piDigitsSync/{digits}")]
        public string GetPiForNDigitsSync(int digits)
        {
            return CalcPiStrSync(digits);
        }

        [HttpGet("piSleep/{digits}/{sleepMs}")]
        public async Task<string> GetPiWithSleep(int digits, int sleepMs)
        {
            await Task.Delay(sleepMs);

            string piStr = await CalcPiStr(digits);

            return piStr;
        }

        [HttpGet("piSleepSync/{digits}/{sleepMs}")]
        public string GetPi10kWithSleepSync(int digits, int sleepMs)
        {
            Thread.Sleep(sleepMs);

            string piStr = CalcPiStrSync(digits);

            return piStr;
        }
    }
}
