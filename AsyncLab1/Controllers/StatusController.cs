﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.AspNetCore.Mvc;

namespace AsyncLab1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StatusController
    {
        public StatusController()
        {
        }

        [HttpGet]
        public ServerInfo Get()
        {
            var info = ThreadStatus();

            return info;
        }

        private ServerInfo ThreadStatus()
        {
            ThreadPool.GetAvailableThreads(out var worker, out var io);
  
            Console.WriteLine("Thread pool threads available at startup: ");
            Console.WriteLine("   Worker threads: {0:N0}", worker);
            Console.WriteLine("   Asynchronous I/O threads: {0:N0}", io);

            ThreadPool.GetMaxThreads(out int nbrWorkers, out int nbrPorts);
            var info = new ServerInfo()
            {
                AvailableIOThreads = io,
                AvailableWorkerThreads = worker,
                MaxWorkerThreads = nbrWorkers,
                MaxCompletionPortThreads = nbrPorts
            };

            ThreadPool.GetMinThreads(out nbrWorkers, out nbrPorts);
            info.MinWorkerThreads = nbrWorkers;
            info.MinCompletionPortThreads = nbrPorts;

            return info;
        }
    }

    public class ServerInfo
    {

        public int AvailableWorkerThreads { get; set; }
        public int MaxWorkerThreads { get; set; }
        public int MinWorkerThreads { get; set; }
        public int AvailableIOThreads { get; set; }
        public int MaxCompletionPortThreads { get; set; }
        public int MinCompletionPortThreads { get; set; }

    }
}