﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AsyncLab1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private static readonly Lazy<IEnumerable<WeatherForecast>> _LazyShortForecast = new Lazy<IEnumerable<WeatherForecast>>(() => BuildForecast(3));
        private static readonly Lazy<IEnumerable<WeatherForecast>> _LazyLongForecast = new Lazy<IEnumerable<WeatherForecast>>(() => BuildForecast(30));

        private readonly ILogger<WeatherForecastController> _logger;
        private static Random _random = new Random();

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            return BuildForecast(5);
        }

        private static IEnumerable<WeatherForecast> BuildForecast(int nbrOfDays)
        {
            var rng = _random;

            return Enumerable.Range(1, nbrOfDays).Select(index => new WeatherForecast
                {
                    Date = DateTime.Now.AddDays(index),
                    TemperatureC = rng.Next(-20, 55),
                    Summary = Summaries[rng.Next(Summaries.Length)]
                })
                .ToArray();
        }

        [HttpGet("short")]
        public async Task<IEnumerable<WeatherForecast>> FakeShortForecast()
        {
            return await Task.FromResult(_LazyShortForecast.Value);
        }

        [HttpGet("shortSync")]
        public IEnumerable<WeatherForecast> FakeShortForecastSync()
        {
            return _LazyShortForecast.Value;
        }

        [HttpGet("long")]
        public async Task<IEnumerable<WeatherForecast>> FakeLongForecast()
        {
            return await Task.FromResult(_LazyShortForecast.Value);
        }

        [HttpGet("longSync")]
        public IEnumerable<WeatherForecast> FakeLongForecastSync()
        {
            return _LazyLongForecast.Value;
        }

        [HttpGet("shortSleep/{sleepMs}")]
        public async Task<IEnumerable<WeatherForecast>> FakeShortForecastWithSleep(int sleepMs)
        {
            await Task.Delay(sleepMs);

            return await Task.FromResult(_LazyShortForecast.Value);
        }

        [HttpGet("shortSleepSync/{sleepMs}")]
        public IEnumerable<WeatherForecast> FakeShortForecastWithSleepSync(int sleepMs)
        {
            Thread.Sleep(sleepMs);

            return _LazyShortForecast.Value;
        }

        [HttpGet("hello")]
        public string Hello()
        {
            return "Hello NBomber!";
        }
    }
}
